// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	int fd;
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void Recibe();
      

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int FIFO_cliente;
	int FIFO_servidor;
	pthread_t thid1;
	pthread_attr_t atrib;
	
	Socket Conexion_Socket;
	Socket Comunicacion_Socket;

       DatosMemCompartida datosCompartidos;	
	DatosMemCompartida *data;	
	int puntos1;
	int puntos2;
	
	typedef struct Puntuaciones
          {
            int jugador1; // Puntos del jugador 1
            int jugador2; // Puntos del jugador 2
            int Ganador;  // Indica quien ha ganado el punto
          }Puntuaciones;
          Puntuaciones puntos;
          
         

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
