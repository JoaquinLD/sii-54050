// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <string>
#include  <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h> 
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d)
{
   CMundoServidor* p = (CMundoServidor*) d;
   p->Recibe();
}


void CMundoServidor::Recibe(){
  // FIFO_cliente =  open ("FIFO_CLIENTE",O_RDONLY);
       
      while(1){
      usleep(10);
      char cadena[100];
      Comunicacion_Socket.Receive(cadena,sizeof(cadena));
      //read(FIFO_cliente,cadena,sizeof(cadena));
      unsigned char key;
      sscanf(cadena,"%c",&key);
      if(key=='s') 
       jugador1.velocidad.y=-4;
      if(key=='w')
       jugador1.velocidad.y=4;
      if(key=='l')
       jugador2.velocidad.y=-4;
      if(key=='o')
       jugador2.velocidad.y=4;
	}
}





CMundoServidor::CMundoServidor()
{
	Init();
	fd = creat ("Tuberia",0777);
}

CMundoServidor::~CMundoServidor()
{
       unlink("Tuberia");
       //close(fd);
     //  close(FIFO_servidor);
     //  close(FIFO_cliente); 
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	


	//Actualizo los datos de la memoria compartida
	//data->esfera=esfera;
	//data->raqueta1=jugador1;
	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera))
	{
	if(jugador1.y2>0.2)
	{
	jugador1.y1=jugador1.y1+0.1f;
	jugador1.y2=jugador1.y2-0.1f;
	}
	}
	if(jugador2.Rebota(esfera))
	{
	if(jugador2.y2>0.2)
	{
	jugador2.y1=jugador2.y1+0.1;
	jugador2.y2=jugador2.y2-0.1;
	}
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;	
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.Ganador=1;
		write(fd,&puntos, sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;	
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.Ganador=2;
		write(fd,&puntos, sizeof(puntos));
	}
	
	
	//Control del bot
	/*
	if(data->accion==-1)
	{
	 OnKeyboardDown ('s',0,0);
	}
	if(data->accion==1)
	{
	 OnKeyboardDown ('w',0,0);
	}
	if(data->accion==0)
	{
	}
	*/
        if(puntos1==3 || puntos2==3)
        {
        if(puntos1==3)
        {
         printf("Gana jugador 1 por llegar a 3 puntos \n");
        }
        else if(puntos2==3)
        {
         printf("Gana jugador 2 por llegar a 3 puntos \n");
        }
        else{}
        exit(0);
        }
        
        char cadena[200];
	sprintf(cadena,"%f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x,esfera.centro.y, 
		jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,
		jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, 
		puntos1, puntos2);
	//write(FIFO_servidor,cadena,sizeof(cadena));
        Comunicacion_Socket.Send(cadena,sizeof(cadena));
        
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
        /*
        
	switch(key)
	{
	case 'a':jugador1.velocidad.x=-1;break;
	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	*/
	
}

void CMundoServidor::Init()
{

     /*
     if((fd=open("FIFO", O_WRONLY))<0)
     {
     perror("Error, no se puede abrir FIFO");
     return;
     }
    
     int fd_mmap;
	char *pfd;
	fd_mmap= open("datosCompartidos", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fd_mmap < 0) {
           perror("Error creación de Datos Compartidos");
           exit(1);
        }
	datosCompartidos.esfera=esfera;
	datosCompartidos.raqueta1=jugador1;
	datosCompartidos.accion=0;
	write(fd_mmap,&datosCompartidos,sizeof(datosCompartidos));
	data=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(datosCompartidos), 
						PROT_READ|PROT_WRITE, MAP_SHARED, fd_mmap, 0));

	
	if (data==MAP_FAILED){
		perror("Error en la proyeccion de Datps compartidos");
		close(fd_mmap);
		return;
	}
	close(fd_mmap);
	*/
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
    /* if((fd=open("FIFO", O_WRONLY))<0)
     {
     perror("Error, no se puede abrir FIFO");
     return;
     }
	*/
   //  FIFO_servidor= open ("FIFO_SERVIDOR", O_WRONLY);
	
	/*if((FIFO_servidor=open("FIFO", O_WRONLY))<0)
     {
     perror("Error, no se puede abrir FIFO_servidor");
     return;
     }   */
     
     char ip[]="127.0.0.1";
     //char ip[]="10.0.2.15";
     if(Conexion_Socket.InitServer(ip,8000)==-1)
     {
        printf("Error al abrir servidor\n");
     }
     Comunicacion_Socket=Conexion_Socket.Accept();
     char nombre[100];
     Comunicacion_Socket.Receive(nombre,sizeof(nombre));
     printf("Ha entrado en partida %s \n",nombre);
     
     
	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &atrib, hilo_comandos, this);
	
}
