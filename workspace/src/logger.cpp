#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>

typedef struct Puntuaciones
{
int jugador1; // Puntos del jugador 1
int jugador2; // Puntos del jugador 2
int Ganador;  // Indica quien ha ganado el punto
}Puntuaciones;

int main()
{

int fd;
int var1;
int var2;
Puntuaciones puntos;

var1=mkfifo("FIFO",0600)<0;
if(var1<0)     
 {
  perror("Error al crear FIFO");
  return 1;
 }
//FIFO creado correctamente, procedemos a abrirlo

fd=open("FIFO",O_RDONLY);
if(fd<0)
  {
  perror("Error al abrir FIFO");
  }
//FIFO abierto correctamente en este punto

while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos)) 
 {
   if(puntos.Ganador==1){
	printf("Jugador 1 anota, Puntos totales = %d puntos.\n",puntos.jugador1);
                  }
   else if(puntos.Ganador==2){
	printf("Jugador 2 anota, Puntos totales = %d puntos.\n",puntos.jugador2);
		              }
}
close(fd);

var2=unlink("FIFO");
if(var2<0)
{
  perror("Error al cerrar FIFO");
} 

return(0);
}
